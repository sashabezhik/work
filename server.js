const express = require('express')
const cors = require('cors')

const app = express()

app.use(cors())
app.use(express.json())

app.post('/', (req, res) => {
    res.json({ message: 'Success!' })
})

app.listen(process.env.PORT || 3000)
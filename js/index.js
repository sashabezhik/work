const buyBtn = document.getElementById('buy')
const modalWindow = document.getElementById('modal')
const modalSuccessWindow = document.getElementById('modal-success')
const closeModalBtn = document.getElementById('close')
const closeSuccessBtn = document.getElementById('close-success')
const sendBtn = document.getElementById('send')

buyBtn.addEventListener('click', () => {
    modalWindow.style.display = 'block'
})

closeModalBtn.addEventListener('click', () => {
    modalWindow.style.display = 'none'
})

closeSuccessBtn.addEventListener('click', () => {
    modalSuccessWindow.style.display = 'none'
})

sendBtn.addEventListener('click', async () => {

    const inputName = document.getElementById('input-name').value
    const inputPhone = document.getElementById('input-phone').value

    if (!inputName && inputPhone) {
        alert('Заполните оба поля')
        return
    } 

    try {
        const response = await fetch('https://bezhik-work-test.herokuapp.com', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: inputName,
                phone: inputPhone
            })
        })

        modalWindow.style.display = 'none'
        modalSuccessWindow.style.display = 'block'
    } catch (error) {
        alert(error)
    }


})